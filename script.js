/* Відповіді:
1. Кожен об'єкт в js має приховану властивість Prototype. Ми можемо
вписати у властивість Prototype одного об'єкту посилання на інший об'єкт і при виклиці першого
ми побачимо властивості другого. Таким чином перший об'єкт унаслідував властивості другого.
2. Для того, щоб не повторювати конструктор батьківського класа, якщо ми хочемо додати інші властивості
до класу-нащадка.
*/

class Employee {
    constructor (name, age, salary){
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name(){
        return this._name;
    }
    set name(newName){
        this._name = newName;
    }
    get age(){
        return this._age;
    }
    set age(newAge){
        this._age = newAge;
    }
    get salary(){
        return this._asalary;
    }
    set salary(newSalary){
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary(){
        return this._salary * 3;
        
    }
    set salary(newSalary){
        this._salary = newSalary;
    }

}

const programmer1 = new Programmer('Maxim', 26, 2500, "English");
const programmer2 = new Programmer('Oleh', 35, 3600, "German");
const programmer3 = new Programmer('Andriy', 19, 1800, "English");
const programmer4 = new Programmer('Mykita', 29, 3000, "French");
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
console.log(programmer4);